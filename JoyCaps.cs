﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace OTech.Joystick
{
	public enum MmResult
	{
		NoError = 0,
		NoDriver = 6,
		InvalParam = 11,
		BadDeviceId = 2,
		ErrorParams = 165,
		UnPlugged = 167
	}

	#region JoyCaps

	/// <summary>
	/// ジョイスティックの追加情報を定義します。
	/// </summary>
	[Flags]
	public enum JoyCapsInfo : uint
	{
		/// <summary>
		/// デバイスに Z 軸があります。
		/// </summary>
		HasZ = 1,
		/// <summary>
		/// デバイスに R 軸があります。
		/// </summary>
		HasR = 2,
		/// <summary>
		/// デバイスに U 軸があります。
		/// </summary>
		HasU = 4,
		/// <summary>
		/// デバイスに V 軸があります。
		/// </summary>
		HasV = 8,
		/// <summary>
		/// デバイスにハット スイッチがあります。
		/// </summary>
		HasPOV = 16,
		/// <summary>
		/// ハット スイッチは 4 方向のみ認識します。
		/// </summary>
		POV4DIR = 32,
		/// <summary>
		/// ハット スイッチはベアリングです。
		/// </summary>
		POVCTS = 64
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct JoyCaps
	{
		/// <summary>
		/// 製造 ID。
		/// </summary>
		public ushort wMid;
		/// <summary>
		/// 製品 ID。
		/// </summary>
		public ushort wPid;
		/// <summary>
		/// 製品名。
		/// </summary>
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
		public string szPname;
		/// <summary>
		/// X 軸の最小値。通常は方向キーを左いっぱいに傾けた時の値です。
		/// </summary>
		public uint wXmin;
		/// <summary>
		/// X 軸の最大値。通常は方向キーを右いっぱいに傾けた時の値です。
		/// </summary>
		public uint wXmax;
		/// <summary>
		/// Y 軸の最小値。通常は方向キーを上いっぱいに傾けた時の値です。
		/// </summary>
		public uint wYmin;
		/// <summary>
		/// Y 軸の最大値。通常は方向キーを下いっぱいに傾けた時の値です。
		/// </summary>
		public uint wYmax;
		/// <summary>
		/// Z 軸の最小値。
		/// </summary>
		public uint wZmin;
		/// <summary>
		/// Z 軸の最大値。
		/// </summary>
		public uint wZmax;
		/// <summary>
		/// デバイスで利用可能なボタンの数。
		/// </summary>
		public uint wNumButtons;
		/// <summary>
		/// <see cref="OTech.Joystick.joySetCapture"/> で指定できる最小のポーリング頻度。ここでは使用しません。
		/// </summary>
		public uint wPeriodMin;
		/// <summary>
		/// <see cref="OTech.Joystick.joySetCapturejoySetCapture"/> で指定できる最大のポーリング頻度。ここでは使用しません。
		/// </summary>
		public uint wPeriodMax;
		/// <summary>
		/// R 軸の最小値。
		/// </summary>
		public uint wRmin;
		/// <summary>
		/// R 軸の最大値。
		/// </summary>
		public uint wRmax;
		/// <summary>
		/// U 軸の最小値。
		/// </summary>
		public uint wUmin;
		/// <summary>
		/// U 軸の最大値。
		/// </summary>
		public uint wUmax;
		/// <summary>
		/// V 軸の最小値。
		/// </summary>
		public uint wVmin;
		/// <summary>
		/// V 軸の最大値。
		/// </summary>
		public uint wVmax;
		/// <summary>
		/// デバイスが、X と Y 軸以外の軸が利用可能、またハット スイッチが利用できるか。
		/// </summary>
		public JoyCapsInfo wCaps;
		/// <summary>
		/// システムが認識できる軸の最大数。
		/// </summary>
		public uint wMaxAxes;
		/// <summary>
		/// デバイスが利用できる軸の数。
		/// </summary>
		public uint wNumAxes;
		/// <summary>
		/// システムが認識できるボタンの最大数。
		/// </summary>
		public uint wMaxButtons;
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
		public string szRegKey;
		[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
		public string szOEMVxD;
	}

	#endregion

#region JoyInfoEx

	public enum JoyInfoExFlags : uint
	{
		ReturnX = 0x1,
		ReturnY = 0x2,
		ReturnZ = 0x4,
		ReturnR = 0x8,
		ReturnU = 0x10,
		ReturnV = 0x20,
		ReturnPOV = 0x40,
		ReturnButtons = 0x80,
		ReturnRawData = 0x100,
		ReturnPovCts = 0x200,
		ReturnCentered = 0x400,
		ReturnAll = (ReturnX | ReturnY | ReturnZ | ReturnR | ReturnU | ReturnV | ReturnPOV | ReturnButtons)
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct JoyInfoEx
	{
		/// <summary>
		/// この構造体のサイズ。
		/// </summary>
		public uint dwSize;
		/// <summary>
		/// 取得するデバイスのデータ。
		/// </summary>
		public JoyInfoExFlags dwFlags;
		/// <summary>
		/// X 軸の現在値。
		/// </summary>
		public uint dwXpos;
		/// <summary>
		/// Y 軸の現在値。
		/// </summary>
		public uint dwYpos;
		/// <summary>
		/// Z 軸の現在値。
		/// </summary>
		public uint dwZpos;
		/// <summary>
		/// R 軸の現在値。
		/// </summary>
		public uint dwRpos;
		/// <summary>
		/// U 軸の現在値。
		/// </summary>
		public uint dwUpos;
		/// <summary>
		/// V 軸の現在値。
		/// </summary>
		public uint dwVpos;
		/// <summary>
		/// 押されているボタン。
		/// </summary>
		public uint dwButtons;
		/// <summary>
		/// 押されているボタンの数。
		/// </summary>
		public uint dwButtonNumber;
		/// <summary>
		/// ハット スイッチの現在値。
		/// </summary>
		public uint dwPov;
		public uint dwReserved1;
		public uint dwReserved2;
	}

#endregion
}
