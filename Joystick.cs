﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OTech.Joystick.Exceptions;
using System.Threading;
using System.Runtime.InteropServices;

namespace OTech.Joystick
{
	/// <summary>
	/// ジョイスティックを使用できるようにします。
	/// </summary>
	public class Joystick
	{
		[DllImport("winmm.dll", CharSet = CharSet.Ansi)]
		private extern static MmResult joyGetPosEx(uint uJoyId, ref JoyInfoEx pji);
		[DllImport("winmm.dll")]
		private extern static uint joyGetNumDevs();
		[DllImport("winmm.dll", CharSet = CharSet.Ansi)]
		private extern static MmResult joyGetDevCaps(uint uJoyId, ref JoyCaps pjc, uint cbjc);

		JoyCaps devData;
		uint _id;

        /// <summary>
        /// 現在使用できるジョイスティックの数を取得します。
        /// </summary>
	    public static int DeviceCount
	    {
	        get { return (int)joyGetNumDevs(); }
	    }

        /// <summary>
        /// 現在使用できるジョイスティックを列挙します。
        /// </summary>
	    public static IEnumerable<Joystick> Joysticks
	    {
	        get
	        {
	            for (uint i = 0; i < DeviceCount; i++)
	                yield return new Joystick(i);
	        }
	    }

	    /// <summary>
	    /// ジョイスティックのデバイス名を取得します。
	    /// </summary>
	    public string DeviceName
	    {
	        get { return this.devData.szPname; }
	    }

		/// <summary>
		/// システムが認識できるボタンの最大数を取得します。
		/// </summary>
		public int MaximumButtons
		{
			get { return (int)this.devData.wNumButtons; }
		}

		/// <summary>
        /// デバイスが使用できるボタンの数を取得します。
        /// </summary>
	    public int Buttons
	    {
	        get { return (int)this.devData.wNumButtons; }
	    }

        /// <summary>
        /// 方向キーを左いっぱいに傾けた時の値を取得します。
        /// </summary>
	    public int MinimumX
	    {
	        get { return (int)this.devData.wXmin; }
	    }

        /// <summary>
        /// 方向キーを右いっぱいに傾けたときの値を取得します。
        /// </summary>
	    public int MaximumX
	    {
	        get { return (int)this.devData.wXmax; }
	    }

        /// <summary>
        /// 方向キーを上いっぱいに傾けたときの値を取得します。
        /// </summary>
        public int MinimumY
        {
            get { return (int)this.devData.wYmin; }
        }

        /// <summary>
        /// 方向キーを下いっぱいに傾けたときの値を取得します。
        /// </summary>
        public int MaximumY
        {
            get { return (int)this.devData.wYmax; }
        }

        /// <summary>
        /// Z 軸の最小値を取得します。
        /// </summary>
	    public int MinimumZ
	    {
	        get { return (int) this.devData.wZmin; }
	    }

	    /// <summary>
	    /// Z 軸の最大値を取得します。
	    /// </summary>
	    public int MaxmumZ
	    {
	        get { return (int)this.devData.wZmax; }
	    }

		/// <summary>
		/// Z 軸の最小値を取得します。
		/// </summary>
		public int MinimumR
		{
			get { return (int)this.devData.wRmin; }
		}

		/// <summary>
		/// Z 軸の最大値を取得します。
		/// </summary>
		public int MaxmumR
		{
			get { return (int)this.devData.wRmax; }
		}

		/// <summary>
		/// U 軸の最小値を取得します。
		/// </summary>
		public int MinimumU
		{
			get { return (int)this.devData.wUmin; }
		}

		/// <summary>
		/// U 軸の最大値を取得します。
		/// </summary>
		public int MaxmumU
		{
			get { return (int)this.devData.wUmax; }
		}

		/// <summary>
		/// V 軸の最小値を取得します。
		/// </summary>
		public int MinimumV
		{
			get { return (int)this.devData.wVmin; }
		}

		/// <summary>
		/// V 軸の最大値を取得します。
		/// </summary>
		public int MaxmumV
		{
			get { return (int)this.devData.wVmax; }
		}

		/// <summary>
		/// デバイスが利用できる入力について取得します。
		/// </summary>
		public JoyCapsInfo Caps
		{
			get { return this.devData.wCaps; }
		}

		/// <summary>
		/// システムが認識できる軸の最大数を取得します。
		/// </summary>
		public int MaxmimumAxes
		{
			get { return (int)this.devData.wMaxAxes; }
		}

		/// <summary>
		/// デバイスが利用できる軸の数を取得します。
		/// </summary>
		public int Axes
		{
			get { return (int)this.devData.wNumAxes; }
		}

		public string RegKey
		{
			get { return this.devData.szRegKey; }
		}

		public string OemVxD
		{
			get { return this.devData.szOEMVxD; }
		}

		/// <summary>
		/// ジョイスティックを使用します。
		/// </summary>
		/// <param name="useJoyId">使用するジョイスティックのデバイス ID。何も指定しない場合は 0 になります。</param>
		public Joystick(uint useJoyId = 0)
		{
			if (joyGetNumDevs() <= 0)
				throw new JoystickException("ジョイスティックを使用できません。");

			MmResult result;
			if ((result = joyGetDevCaps(useJoyId, ref this.devData, (uint)Marshal.SizeOf(devData))) > 0)
			{
				var restr = string.Empty;
				switch (result)
				{
					case MmResult.NoDriver:
						restr = "ジョイスティックのドライバーがインストールされていません。";
						break;
					case MmResult.InvalParam:
						restr = "無効なパラメーターが指定されました。";
						break;
					case MmResult.BadDeviceId:
						restr = "デバイス IDが無効です。";
						break;
					case MmResult.UnPlugged:
						restr = "ジョイスティックが接続されていません。";
						break;
					case MmResult.ErrorParams:
						restr = "指定された useJoyId は無効です。";
						break;
					default:
						break;
				}
				throw new JoystickException("ジョイパッドの情報取得に失敗しました。" + restr + " エラー番号: " + (int)result);
			}

			_id = useJoyId;
		}

		/// <summary>
		/// ジョイスティックの入力値を取得します。
		/// </summary>
		/// <returns></returns>
		public JoystickState GetJoystick()
		{
			JoyInfoEx jie = new JoyInfoEx();
			jie.dwFlags = JoyInfoExFlags.ReturnAll;
			jie.dwSize = (uint)Marshal.SizeOf(jie);
			joyGetPosEx(_id, ref jie);

			return new JoystickState(jie, devData);
		}
	}
}
