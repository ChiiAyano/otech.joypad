﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OTech.Joystick
{
	/// <summary>
	/// ジョイ パッドのキーの状態を取得します。
	/// </summary>
	public class JoystickState
	{
		#region ボタン

		[Flags]
		public enum ButtonState
		{
			Button1 = 1,
			Button2 = 2,
			Button3 = 4,
			Button4 = 8,
			Button5 = 16,
			Button6 = 32,
			Button7 = 64,
			Button8 = 128,
			Button9 = 256,
			Button10 = 512,
			Button11 = 1024,
			Button12 = 2048,
			Button13 = 4096,
			Button14 = 8192,
			Button15 = 16384,
			Button16 = 32768
		}

		/// <summary>
		/// 押しているボタンを取得します。
		/// </summary>
		public ButtonState Buttons
		{ get; private set; }

		/// <summary>
		/// 押しているボタンの数を取得します。
		/// </summary>
		public int ButtonCount
		{ get; private set; }

		/// <summary>
		/// 全ての押しているボタンを取得します。16 個以上ボタンがあるジョイスティックを扱う場合に利用してください。
		/// </summary>
		/// <remarks>得られる数は 1 ボタンから順に 2 の累乗 (1, 2, 4, 8, 16 ...) です。複数押されている場合は論理演算された値が返ります。</remarks>
		public int ButtonAll
		{ get; private set; }

		#endregion

		#region カーソルキー

		/// <summary>
		/// ハット スイッチの押された向きを定義します。
		/// </summary>
		[Flags()]
		public enum Curcor : byte
		{
			/// <summary>
			/// 何も押されていません。
			/// </summary>
			None = 0,
			/// <summary>
			/// 上方向にカーソルが押されました。
			/// </summary>
			Up = 1,
			/// <summary>
			/// 下方向にカーソルが押されました。
			/// </summary>
			Down = 2,
			/// <summary>
			/// 左方向にカーソルが押されました。
			/// </summary>
			Left = 4,
			/// <summary>
			/// 右方向にカーソルが押されました。
			/// </summary>
			Right = 8
		}

		/// <summary>
		/// ハット スイッチの押された向きを取得します。
		/// </summary>
		public Curcor POV
		{ get; private set; }

		#endregion

		#region スティック

		/// <summary>
		/// スティックの傾いた向きを取得します。2 つスティックがあるデバイスでは、通常左側のスティックです。
		/// </summary>
		public Curcor Stick1
		{ get; private set; }

		/// <summary>
		/// スティックの傾いた向きを取得します。2 つスティックがあるデバイスでは、通常右側のスティックです。
		/// </summary>
		public Curcor Stick2
		{ get; private set; }

		#endregion

		#region 生データ

		/// <summary>
		/// ジョイスティックの情報を取得します。
		/// </summary>
		public JoyCaps JoyCaps
		{ get; private set; }

		/// <summary>
		/// ジョイスティックの現在の状態を取得します。
		/// </summary>
		public JoyInfoEx JoyInfo
		{ get; private set; }

		#endregion

		public JoystickState(JoyInfoEx state, JoyCaps caps)
		{
			#region 生データ

			JoyCaps = caps;
			JoyInfo = state;

			#endregion

			#region ボタン

			Buttons = (ButtonState)state.dwButtons;
			ButtonAll = (int)state.dwButtons;
			ButtonCount = (int)state.dwButtonNumber;

			#endregion

			#region POV

			switch (state.dwPov / 100)
			{
				case 0:
					// 上方向
					POV = Curcor.Up;
					break;
				case 45:
					// 右上方向
					POV = Curcor.Up | Curcor.Right;
					break;
				case 90:
					// 右方向
					POV = Curcor.Right;
					break;
				case 135:
					// 右下方向
					POV = Curcor.Right | Curcor.Down;
					break;
				case 180:
					// 下方向
					POV = Curcor.Down;
					break;
				case 225:
					// 左下方向
					POV = Curcor.Down | Curcor.Left;
					break;
				case 270:
					// 左方向
					POV = Curcor.Left;
					break;
				case 315:
					// 左上方向
					POV = Curcor.Left | Curcor.Up;
					break;
				default:
					// 入力無し
					POV = Curcor.None;
					break;
			}

			#endregion

			#region スティック1

			uint left = caps.wXmin + (caps.wXmax - caps.wXmin) / 3;
			uint top = caps.wYmin + (caps.wYmax - caps.wYmin) / 3;
			uint right = caps.wXmax - (caps.wXmax - caps.wXmin) / 3;
			uint bottom = caps.wYmax - (caps.wYmax - caps.wYmin) / 3;


			if (state.dwYpos < top)
			{
				if (state.dwXpos < left)
					Stick1 = Curcor.Up | Curcor.Left; // 左上
				else if (state.dwXpos > right)
					Stick1 = Curcor.Up | Curcor.Right; // 右上
				else
					Stick1 = Curcor.Up; // 上
			}
			else if (state.dwYpos > bottom)
			{
				if (state.dwXpos < left)
					Stick1 = Curcor.Down | Curcor.Left; // 左下
				else if (state.dwXpos > right)
					Stick1 = Curcor.Down | Curcor.Right; // 右下
				else
					Stick1 = Curcor.Down; // 下
			}
			else if (state.dwXpos < left) // 左
				Stick1 = Curcor.Left;
			else if (state.dwXpos > right) // 右
				Stick1 = Curcor.Right;
			else
				Stick1 = Curcor.None;


			#endregion

			#region スティック2

			uint left2 = caps.wZmin + (caps.wZmax - caps.wZmin) / 3;
			uint top2 = caps.wRmin + (caps.wRmax - caps.wRmin) / 3;
			uint right2 = caps.wZmax - (caps.wZmax - caps.wZmin) / 3;
			uint bottom2 = caps.wRmax - (caps.wRmax - caps.wRmin) / 3;


			if (state.dwRpos < top2)
			{
				if (state.dwZpos < left2)
					Stick2 = Curcor.Up | Curcor.Left; // 左上
				else if (state.dwZpos > right2)
					Stick2 = Curcor.Up | Curcor.Right; // 右上
				else
					Stick2 = Curcor.Up; // 上
			}
			else if (state.dwRpos > bottom2)
			{
				if (state.dwZpos < left2)
					Stick2 = Curcor.Down | Curcor.Left; // 左下
				else if (state.dwZpos > right2)
					Stick2 = Curcor.Down | Curcor.Right; // 右下
				else
					Stick2 = Curcor.Down; // 下
			}
			else if (state.dwZpos < left2) // 左
				Stick2 = Curcor.Left;
			else if (state.dwZpos > right2) // 右
				Stick2 = Curcor.Right;
			else
				Stick2 = Curcor.None;


			#endregion
		}
	}
}
