﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OTech.Joystick.Exceptions
{
	public class JoystickException : Exception
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="message"></param>
		/// <param name="result"></param>
		public JoystickException(string message)
			: base(message)
		{
		}
	}
}
